package springFront;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import services.ServiceException;
import services.TweetService;
import services.UserService;



@Controller
public class APIController {

	private static final Logger logger = LoggerFactory.getLogger(APIController.class);

	@Autowired TweetService tweetService;
	@Autowired UserService userService;

	@RequestMapping(value = "/tweets", method={RequestMethod.GET}, produces="application/json")
	public ResponseEntity<Object> getTweets() {
		logger.info("Calling /api/tweets");		
		try {
			return new ResponseEntity<Object>(tweetService.findAll(), HttpStatus.OK );
		}
		catch (ServiceException ex) {
			return new ResponseEntity<Object>(ex.getMessageList(), HttpStatus.INTERNAL_SERVER_ERROR);		}
	}

	@RequestMapping(value = "/tweets/{tweetID}", method = RequestMethod.PUT,
			consumes="application/json", produces="application/json")
	public ResponseEntity<Object> like(@RequestBody Map<String,Object> tweet) {

		try {
			tweetService.update((Integer) tweet.get("tweetID"), (Integer) tweet.get("likes"));
			logger.info("Updating Tweet with id = "+tweet.get("tweetID")) ;
			return new ResponseEntity<Object>(tweet, HttpStatus.OK );
		}
		catch (Exception ex) {
			return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST,
			consumes="application/json", produces="application/json")
	public ResponseEntity<Object> login(@RequestBody Map<String,Object> user) {

		logger.info("Logging User with username = "+user.get("username")) ;

		String username = (String) user.get("username");
		String password = (String) user.get("password");

		try {
			user.put("userID",userService.login(username, password));
			logger.info("User successfully logged");
			return new ResponseEntity<Object>(user, HttpStatus.OK );
		}
		catch (ServiceException ex) {
			String exMess = ex.getMessageList().elementAt(0);
			String errMess;
			if (exMess.startsWith("login")){
				if (exMess.equals("loginUser")) {
					errMess = "There is no such a user";
				}
				else {
					errMess = "Wrong password!";
				}
				logger.info("Login fais after DB check: "+errMess);
				return new ResponseEntity<Object>(errMess, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			else {
				return new ResponseEntity<Object>(ex.getMessageList(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/tweets", method = RequestMethod.POST,
			consumes="application/json", produces="application/json")
	public ResponseEntity<Object> newTweet(@RequestBody Map<String,Object> tweet) {

		logger.info("Adding new tweet with content = "+tweet.get("content")) ;
		Integer userID = (Integer)tweet.get("authorID");
		String content = (String)tweet.get("content");
		String authorName = (String)tweet.get("authorName");

		try {
			tweet = (Map<String,Object>) tweetService.create(userID, content);
			tweet.put("authorName", authorName);
			logger.info("The new tweet #"+tweet.get("tweetID")+" has the following content: "+content) ;
			return new ResponseEntity<Object>(tweet, HttpStatus.OK );
		}
		catch (ServiceException ex) {
			return new ResponseEntity<Object>(ex.getMessageList(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
