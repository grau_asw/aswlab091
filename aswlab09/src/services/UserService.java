package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import util.MD5Encoder;

@Service
public class UserService extends AppService {

	public Integer /*userID*/ login(String username, String password) throws ServiceException {
		Connection con = this.getDBConnection();
		Integer userID = -1;
		try {
			String  queryString = "select usID,(? = usPASSW) from users where usNAME = ?";
			PreparedStatement stmt = con.prepareStatement(queryString);
			stmt.setString(1, MD5Encoder.encode(password));
			stmt.setString(2, username);
			ResultSet rs = stmt.executeQuery();
			if (rs.first())
				if (rs.getBoolean(2)) userID = (Integer) rs.getInt(1);
				else throw new ServiceException("loginPassword");
			else
				throw new ServiceException("loginUser");
			rs.close();
			stmt.close();
		}
		catch (SQLException ex ) {throw new ServiceException(ex);
		}
		return userID;
	}


}
