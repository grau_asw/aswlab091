package services;

import java.sql.Connection;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class AppService {

	@Autowired ServletContext servletContext = null;

	protected Connection getDBConnection() {
		return (Connection) servletContext.getAttribute("dbConnection");
	}


}
