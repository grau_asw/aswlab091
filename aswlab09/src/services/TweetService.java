package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Vector;

import org.springframework.stereotype.Service;

import util.TimeService;

@Service
public class TweetService extends AppService {
	
	public Vector<HashMap<String, Object>> /*Tweet List*/ findAll() throws ServiceException {

		Vector<HashMap<String, Object>> wallTweets = new Vector<HashMap<String, Object>>();
		Connection con = this.getDBConnection();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Statement stmt = con.createStatement();
			String query = "select t.*, u.usNAME from tweets t natural join users u order by twTIME";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				HashMap<String,Object> tweetHash = new HashMap<String, Object>();
				Integer tweetid = new Integer(rs.getInt(1));
				tweetHash.put("tweetID", tweetid);
				tweetHash.put("authorID", rs.getString(2));
				tweetHash.put("content", rs.getString(3));
				tweetHash.put("likes", rs.getString(4));
				java.util.Date temps = mySQLTimeStamp.parse(rs.getString(5), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				String authorName = rs.getString(6);
				tweetHash.put("authorName", authorName);
				wallTweets.addElement(tweetHash);
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException ex) { throw new ServiceException(ex);}
		return wallTweets;
	}
	
	public void update(Integer tweetID, Integer likes) throws ServiceException
	{
		Connection con = this.getDBConnection();
		String update = "update tweets set twLIKES = " + likes + " where twID = "+tweetID;
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate(update);
		}
		catch (SQLException ex) { throw new ServiceException(ex);}
	}
	
	public HashMap<String,Object> /*Tweet*/ create(Integer userID, String content) throws  ServiceException
	{
		Connection con = this.getDBConnection();
		String insert = "insert into tweets (usID, twTEXT) values (?, ?)";
		HashMap<String,Object> tweetHash = new HashMap<String, Object>();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			PreparedStatement stmt = con.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, userID.intValue());
			stmt.setString(2, content);
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs != null && rs.next()) { 
				Statement stmt2 = con.createStatement();
				String query = "select * from tweets where twID = " + rs.getLong(1);
				ResultSet rs2 = stmt2.executeQuery(query);
				rs2.first();
				Integer tweetid = new Integer(rs2.getInt(1));
				tweetHash.put("tweetID", tweetid);
				tweetHash.put("authorID", rs2.getString(2));
				tweetHash.put("content", rs2.getString(3));
				tweetHash.put("likes", rs2.getString(4));
				java.util.Date temps = mySQLTimeStamp.parse(rs2.getString(5), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				rs2.close();
				stmt2.close();
			}
			rs.close();
			stmt.close();

		}
		catch (SQLException ex) { throw new ServiceException(ex);}
		return tweetHash;
	}
}
